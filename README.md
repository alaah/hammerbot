# About
Promise-based multi-server discord.js bot made for the [Dead Cells server](https://discord.gg/deadcells).

# Features
* A bunch of Dead Cells content/fluff.
* Duel Rock-Paper-Scissors minigame.
* Submitting nice ordered embeds: 

![Example embed](./files/examples/submission.png).

# Authors
Made by alaah#2380

Everything in src is licensed under AGPL. See: LICENSE.

files dir contains a bunch of random content from the internet, own work and data ripped from Dead Cells.