// hammerbot: Dead Cells Discord bot
// Copyright (C) 2018 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Discord = require('discord.js');
const Tools = require('../../util/tools.js');
const Permissions = require('../../util/permissions.js');

module.exports = {
    trigger: 'update',
    description: 'Updates *all* scores in the database. For internal use.',
};

let updateRow = (context, msg, chan, row, score) => {
    chan.fetchMessage(row.msgid).then(parsed => {
        let query = 'update data set score = ' + (score ? Tools.getScore(parsed, row).toString() : '-10000') + ' where id = ' + row.id + ';';
        context.postgres.query(query, (err, res) => {
            if(err) {
                console.log(err);
            }
        });
    }).catch((error) => {
        if(error.message === 'Unknown Message') {
            console.log('Message ' + row.id + ' exists in the database but not on the server.');
            // context.postgres.query('delete from data where id = ' + row.id + ';', (err, res) => {
                // if(err) {
                    // console.log(err);
                // }
            // });
        } else {
            console.log(error);
        }
    });
}

module.exports.exec = (msg, context) => {
    if(!Permissions.isMaster(msg.author)) {
        msg.channel.send('No.');
    } else {
        msg.channel.send('Updating db.');
        context.postgres.query('select * from data order by id desc;', (err, res) => {
            if(err) {
                console.log('Whoops. ' + err.stack);
                msg.channel.send('I was unable to query at the moment.');
            } else {
                res.rows.forEach((row) => {
                    if(row.type < 2) {
                        updateRow(context, msg, context.channels.suggestion, row, true);
                    } else {
                        updateRow(context, msg, context.channels.bug, row, false);
                    }
                });
            }
        });
    }
};
