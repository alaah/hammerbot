// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Hammer = require('../../core/hammer.js');

module.exports = {
    trigger: 'end',
    description: 'Gracefully ends the process.',
    perm: 3
}

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        if(Permissions.isMaster(context.author)) {
            Hammer.client.destroy().then(process.exit()).catch(reject);
        } else {
            resolve('You can\'t stop me, prisoner.');
        }
    });
};
