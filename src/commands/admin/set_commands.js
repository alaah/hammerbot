// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Hammer = require('../../core/hammer.js');

module.exports = {
    trigger: 'set_commands',
    description: 'Set command triggers for a submission channel.',
    params: [
        {
            name: 'id',
            type: 'int',
            description: 'id of the command entry',
            optional: false
        },
        {
            name: 'triggers',
            type: 'clear',
            description: 'triggers separated with spaces',
            optional: false
        }
    ],
    dm: -1,
    perm: 2
};

module.exports.exec = (context, params) => {
    console.log(`update channels set triggers='{${params.triggers.replace(' ', ',')}}' where localid=${params.id} and serverid=${context.server.id};`);
    return new Promise((resolve, reject) => {
        Hammer.postgres.query(`update channels set triggers='{${params.triggers.replace(' ', ',')}}' where localid=${params.id} and serverid='${context.server.id}';`, (err, res) => {
            if(err) {
                console.error(err);
                reject("Error.");
            } else {
                context.server.reload_channels();
                resolve("Success.");
            }
        });
    });
};
