// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Hammer = require('../../core/hammer.js');

module.exports = {
    trigger: 'set_channel',
    description: 'Add a submission channel.',
    params: [
        {
            name: 'use_voting',
            type: 'int',
            description: '0 for no voting, 1 for voting',
            optional: false
        },
        {
            name: 'channelid',
            type: 'word',
            description: 'channel id',
            optional: false
        },
        {
            name: 'color1',
            type: 'word',
            description: 'embed color when unapproved',
            optional: false
        },
        {
            name: 'color2',
            type: 'word',
            description: 'embed color when approved',
            optional: false
        },
        {
            name: 'name',
            type: 'clear',
            description: 'submission title',
            optional: false
        }
    ],
    dm: -1,
    perm: 2
};

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        const id = context.server.channels.length + 1;
        Hammer.postgres.query(`insert into channels (is_suggestion, channelid, color1, color2, name, serverid, localid) values('${params.use_voting}', '${params.channelid}','${params.color1}','${params.color2}','${params.name}','${context.server.id}',${id});`, (err, res) => {
            if(err) {
                console.error(err);
                reject("Error.");
            } else {
                context.server.reload_channels();
                resolve(`Successfully added an entry with local id ${id}.`);
            }
        });
    });
};
