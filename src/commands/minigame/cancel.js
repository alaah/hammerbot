// hammerbot: Dead Cells Discord bot
// Copyright (C) 2018 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const DuelInstance = require('../../util/duel-instance.js');

module.exports = {
    trigger: 'cancel',
    aliases: [
        {
            alias: 'forfeit',
            hidden: false
        }
    ],
    description: 'Frees up the arena.'
};

module.exports.exec = context => {
    return new Promise((resolve, reject) => {
        let found = false;
        DuelInstance.duels.forEach(duel => {
            if(duel.user[0] === context.author || duel.user[1] === context.author) {
                duel.deleteThis(true);
                resolve();
                found = true;
            }
        });
        if(!found) {
            reject('You\'re not currently in a duel.');
        }
    });
};
