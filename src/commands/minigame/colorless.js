// hammerbot: Dead Cells Discord bot
// Copyright (C) 2018 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const DuelInstance  = require('../../util/duel-instance.js');

module.exports = {
    trigger: 'colorless',
    params: [
        {
            name: 'item',
            type: 'word',
            description: 'part of the name of the item; see !listicons',
            optional: true
        }
    ],
    aliases: [
        {
            alias: 'white',
            hidden: false
        },
        {
            alias: 'yellow',
            hidden: false
        }
    ],
    description: 'Chooses Colorless. Becomes either cursed or legendary.',
    dm: 1
};

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        let duel = DuelInstance.findDuel(context.author);
        if(duel) {
            duel.call(context, 3, params.item);
        } else {
            reject('You must be in the middle of a duel!');
        }
    });
};
