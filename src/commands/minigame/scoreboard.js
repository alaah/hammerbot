// hammerbot: Dead Cells Discord bot
// Copyright (C) 2018 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Discord = require('discord.js');
const Hammer = require('../../core/hammer.js');

module.exports = {
    trigger: 'scoreboard',
    aliases: [
        {
            alias: 'leaderboard',
            hidden: false
        },
        {
            alias: 'leaderboards',
            hidden: true
        }
    ],
    description: 'Lists the top scorers.',
    dm: -1
};

let desc = '';

const printRow = (i, context, rows, embed) => {
    console.log(i);
    return new Promise((resolve, reject) => {
        if(i < 10 && i < rows.length) {
            new Promise((resolve2, reject2) => {
                Hammer.client.fetchUser(rows[i].userid).then(fetched => {
                    desc += '#' + (i + 1) + ': ' + fetched.username + ' [' + rows[i].score + ']\n';
                    resolve2();
                }).catch(reject2);
            }).then(() => {
                printRow(i + 1, context, rows, embed).then(resolve).catch(reject);
            });
        } else {
            embed.setDescription(desc);
            resolve();
        }
    });
};

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        let embed = new Discord.RichEmbed();
        embed.setTitle('Scoreboard');
        desc = '';
        Hammer.postgres.query(`select * from lead where serverid='${context.guild.id}' order by score desc;`, (err, res) => {
            printRow(0, context, res.rows, embed).then(() =>{
                console.log(embed);
                resolve(embed);
            }).catch(err => {
                if(err instanceof String) {
                    reject(err);
                } else {
                    console.error(err);
                    reject("Error.");
                }
            });
        });
    });
};
