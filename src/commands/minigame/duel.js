// hammerbot: Dead Cells Discord bot
// Copyright (C) 2018 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Tools = require('../../util/tools.js');
const LoadIcon = require('../../util/load-icon.js');
const DuelInstance = require('../../util/duel-instance.js');

module.exports = {
    trigger: 'duel',
    params: [
        {
            name: 'options',
            type: 'word',
            description: 'see above',
            optional: true
        },
        {
            name: 'user',
            type: 'word',
            description: '@Mention or Tag#1234',
            optional: false
        }
    ],
    aliases: [
        {
            alias: 'dual',
            hidden: true
        },
        {
            alias: 'battleroyale',
            hidden: false
        },
        {
            alias: 'battleroyal',
            hidden: true
        },
        {
            alias: 'battle',
            hidden: false
        },
        {
            alias: 'br',
            hidden: false
        }
    ],
    description: 'Challenges a user to a rock-paper-scissors duel.',
    detail: 'Reply in DM with your color and item choice and await the result. Scores are ranked and readable with `score` and `leaderboards`.',
    custom_fields: [
        {
            field: 'Options',
            text:
           '**n**ohelp – doesn\'t DM instructions\n\
            **f**antasy items – lets you choose items that don\'t match their in-game colors\n\
            **d**ouble stake – gives ±2 points instead of ±1\n\
            **t**rue RPS – no colorless\n\
            **u**nranked – doesn\'t give points and overrides double stake.\n\
            Only use the bolded letters. If you want several options at once, combine them into one word e.g. `tnf`.'
        },
        {
            field: 'Colors',
            text:
            'Brutality (red) – beats Survival, loses with Tactics\n\
            Tactics (purple) – lose with Survival, beat Brutality\n\
            Survival (green) – loses with Brutality, wins with Tactics\n\
            Colorless (white/yellow) – has a 50% chance to become *legendary* and win, otherwise it is *cursed* and loses; two legendary/cursed weapons will still tie.\n\
            See `!chart`.'
        },
        {
            field: 'Examples',
            text:
           '`purple death` (tactics Death Orb)\n\
            `brutality` (random Brutality item)\n\
            `white bloodsword` (colorless Blood Sword)'
        },
    ],
    dm: -1
};

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        let challenger = context.author;
        Tools.getUserFromMention(params.user).then(challenged => {
            if(challenger === challenged) {
                reject('Cannot challenge yourself.');
            } else {
                let free = true;

                DuelInstance.duels.forEach(duel => {
                    if(duel.user[0] === challenger || duel.user[0] === challenged || duel.user[1] === challenger || duel.user[1] == challenged) {
                        free = false;
                    }
                });

                if(free) {
                    let duel = new DuelInstance(context.channel, challenger, challenged, params.options);

                    duel.sendNotice(context, true);

                    if(!challenged.bot) {
                        duel.sendNotice(context, false);
                    }

                    resolve();
                } else {
                    reject('At least one of the participants is currently in another duel.');
                }
            }
        }).catch((err) => {
            if(err instanceof String) {
                reject(err);
            } else {
                reject("Error.");
                console.error(err);
            }
        });
    });
};
