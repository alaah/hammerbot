// hammerbot: Dead Cells Discord bot
// Copyright (C) 2018 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Duel = require('./duel.js');
const Discord = require('discord.js');

module.exports = {
    trigger: 'leaderboards',
    aliases: [
        {
            alias: 'leaderboard',
            hidden: true
        },
        {
            alias: 'scoreboard',
            hidden: false
        }
    ],
    description: 'Lists the top scorers.'
};

let desc = '';

let printRow = (i, context, rows, embed, channel) => {
    if(i < 10) {
        new Promise((resolve, reject) => {
            context.client.fetchUser(rows[i].userid).then(fetched => {
                desc += '#' + (i + 1) + ': ' + fetched.username + ' [' + rows[i].score + ']\n';
                resolve();
            }).catch(console.log);
        }).then(printRow.bind(null, i + 1, context, rows, embed, channel)).catch(console.log);
    } else {
        embed.setDescription(desc);
        channel.send(embed);
    }
};

module.exports.exec = (msg, context, params) => {
    let embed = new Discord.RichEmbed();
    embed.setTitle('Leaderboards');
    desc = '';
    context.postgres.query('select * from lead order by score desc;', (err, res) => {
        printRow(0, context, res.rows, embed, msg.channel);
    });
};
