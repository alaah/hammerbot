// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Tools = require('../../util/tools.js');
const Server = require('../../core/server.js');
const Hammer = require('../../core/hammer.js');

module.exports = {
    trigger: 'print',
    aliases: [
        {
            alias: 'p',
            hidden: false
        }
    ],
    description: 'Fetches and prints a submission.',
    params: [
        {
            name: 'id',
            type: 'int',
            description: 'id of the submission',
            optional: false
        }
    ]
}

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        Hammer.postgres.query(`select * from data where id=${params.id} and serverid='${context.server.id}'`, (err, res) => {
            if(err) {
                reject(err);
            } else if(res.rows.length > 0) {
                const row = res.rows[0];
                const data = context.server.get_channel(row.type);
                data.channel.fetchMessage(row.msgid).then(parsed => {
                    resolve({
                        embed: {
                            color: parseInt(row.type < 0 ? data.color2 : data.color1, 16),
                            author: {
                                name: data.name + ' by ' + (context.channel.guild.members.has(row.userid) ? context.channel.guild.members.get(row.userid).user.tag : 'a user who has left the server'),
                                icon_url: (context.channel.guild.members.has(row.userid) ? context.channel.guild.members.get(row.userid).user.avatarURL : '')
                            },
                            description: parsed.embeds[0].description,
                            footer: {
                                text: (data.is_suggestion ? 'score: ' + Tools.getScore(parsed, row) + '; ' : '') + 'id: ' + row.id
                            },
                            timestamp: parsed.editedAt ? parsed.editedAt : parsed.createdAt,
                            image: parsed.embeds[0].image ? {
                                url: parsed.embeds[0].image.url
                            } : null,
                            fields: parsed.embeds[0].fields
                        }
                    });
                }).catch(error => {
                    console.error('printFromSQLRow fail\n' + error);
                    reject('An error has occured. This submission was probably deleted.');
                });
            } else {
                reject('The id could not be found.');
            }
        });
    });
};
