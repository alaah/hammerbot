// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Hammer = require('../../core/hammer.js');
const Permissions = require('../../util/permissions.js');
const Parser = require('../../core/command-parser.js');
const Server = require('../../core/server.js');
const Fetch = require('../../util/fetch.js');

module.exports = {
    trigger: 'delete',
    aliases: [
        {
            alias: 'd',
            hidden: false
        },
        {
            alias: 'del',
            hidden: true
        },
        {
            alias: 'rm',
            hidden: true
        }
    ],
    description: 'Removes a submission.',
    params: [
        {
            name: 'id',
            type: 'int',
            description: 'id of the submission',
            optional: false
        }
    ],
    dm: -1
}

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        Fetch(context, params.id).then(data => {
            if(data.row.userid === context.author.id || Permissions.isPrivileged(context)) {
                context.channel.send('Deleting the following submission:');
                Parser.exec(context, 'print', params).then(() => {
                    Hammer.postgres.query(`delete from data where id=${data.id} and serverid='${context.server.id}';`, (err, res) => {
                        if(err) {
                            console.log(err);
                        }
                    });
                    data.post.delete().then(resolve).catch(reject);
                }).catch(reject);
            } else {
                reject('You are not allowed to delete this submission.');
            }
        }).catch(error => {
            if(typeof(error) === 'string') {
                reject(error);
            } else {
                console.error(error);
                reject("Error.")
            }
        });
    });
};
