// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Discord = require('discord.js');
const Tools = require('../../util/tools.js');
const Permissions = require('../../util/permissions.js');
const Fetch = require('../../util/fetch.js');

module.exports = {
    trigger: 'unnoteall',
    aliases: [
        {
            alias: 'a',
            hidden: false
        }
    ],
    description: 'Removes all notes from a submission.',
    params: [
        {
            name: 'id',
            type: 'int',
            description: 'id of the submission',
            optional: false
        }
    ],
    dm: -1,
    props: {
        clean: true
    }
};

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        Fetch(context, params.id).then(data => {
            if(data.row.userid === context.author.id || Permissions.isPrivileged(context)) {
                let embed = new Discord.RichEmbed(data.post.embeds[0]);
                let count = embed.fields.length;
                embed.fields = [];
                data.post.edit(embed);
                resolve('Successfully removed ' + count + ' notes.');
            } else {
                reject('Only the submission\'s author and mods can clear notes.');
            }
        }).catch(error => {
            if(typeof(error) === 'string') {
                context.channel.send(error);
            } else {
                console.log(error);
            }
        });
    });
};
