// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Discord = require('discord.js');
const Tools = require('../../util/tools.js');
const Permissions = require('../../util/permissions.js');
const Fetch = require('../../util/fetch.js');

module.exports = {
    trigger: 'unnote',
    aliases: [
        {
            alias: 'u',
            hidden: false
        }
    ],
    description: 'Removes all notes from a submission sent by a specific user (you by default).',
    params: [
        {
            name: 'id',
            type: 'int',
            description: 'id of the submission',
            optional: false
        },
        {
            name: 'user',
            type: 'word',
            description: '@Mention or Tag#1234',
            optional: true
        }
    ],
    props: {
        clean: true
    }
};

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        Fetch(context, params.id).then(data => {
            let embed = new Discord.RichEmbed(data.post.embeds[0]);
            let old_count = embed.fields.length;
            let unnote = (tag) => {
                embed.fields = Tools.removeFieldsBy(embed.fields, tag);
                data.post.edit(embed);
                resolve('Successfully removed ' + (old_count - embed.fields.length) + ' notes.');
            };

            if(params.user) {
                if(data.row.userid === context.author.id || Permissions.isPrivileged(context)) {
                    context.client.fetchUser(Tools.mentionToID(params.user)).then(user => {
                        unnote(user.tag);
                    }).catch(() => {
                        // Didn't fetch, then it's not a mention => must be a tag.
                        unnote(params.user);
                    });
                } else {
                    reject.send('Only the submission\'s author and mods can clear notes.');
                }
            } else {
                unnote(context.author.tag);
            }
        }).catch(error => {
            if(typeof(error) === 'string') {
                reject.send(error);
            } else {
                reject('Error.');
                console.log(error);
            }
        });
    });
};
