// hammerbot: Dead Cells Discord bot
// Copyright (C) 2018 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Discord = require('discord.js');
const Tools = require('../../util/tools.js');
const Permissions = require('../../util/permissions.js');

module.exports = {
    trigger: 'rank',
    aliases: [
        {
            alias: 'r',
            hidden: false
        }
    ],
    params: [
        {
            name: 'score',
            type: 'int',
            description: 'minimum s',
            optional: false
        },
        {
            name: 'minid',
            type: 'int',
            description: 'starting id; the first suggestion by default',
            optional: true
        },
        {
            name: 'maxid',
            type: 'int',
            description: 'final id; the most recent suggestion by default',
            optional: true
        }
    ],
    description: 'Searches for submissions with a minimum score and within an id range.',
    detail: 'Only looks for game suggestions and orders them by score. When used in a public channel, a maximum of three submissions may be printed.',
    example: 'rank 30 800 1000'
};

module.exports.exec = (msg, context, params) => {
    let threshold = params[0];
    let min_id = params[1] ? params[1] : -1;
    let max_id = params[2] ? params[2] : -1;

    let found = [];

    if(threshold >= 10) {
        let query = 'select * from data where type = 0 and score >= ' + threshold
                     + ' and id > ' + min_id + (max_id > 0 ? ' and id < ' + max_id : '') + ' order by score desc;';
        context.postgres.query(query, (err, res) => {
            if(err) {
                console.log('Whoops. ' + err.stack);
                msg.channel.send('I was unable to query at the moment.');
            } else {
                if(res.rows.length === 0) {
                    msg.channel.send('Nothing found.');
                } else {
                    res.rows.forEach((row, i) => {
                        if(i < 3 || Permissions.isMaster(msg.author) || msg.channel.type === "dm") {
                            Tools.printFromSQLRow(msg, row);
                        }
                    });
                }
            }
        });
    } else {
        msg.channel.send('Have mercy! Score must be at least 10.');
    }
};
