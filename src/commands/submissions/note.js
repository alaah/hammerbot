// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Discord = require('discord.js');
const Fetch = require('../../util/fetch.js');

module.exports = {
    trigger: 'note',
    aliases: [
        {
            alias: 'n',
            hidden: false
        }
    ],
    description: 'Adds a comment under a submission.',
    params: [
        {
            name: 'id',
            type: 'int',
            description: 'id of the submission',
            optional: false
        },
        {
            name: 'content',
            type: 'clear',
            description: 'content of the note',
            optional: false
        }
    ],
    dm: -1,
    props: {
        clean: true
    }
}

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        Fetch(context, params.id).then(data => {
            let embed = new Discord.RichEmbed(data.post.embeds[0]);
            embed.addField('Note by ' + context.author.tag, params.content);
            data.post.edit(embed);
            resolve('Success.');
        }).catch(error => {
            if(typeof(error) === 'string') {
                console.log("xd");
                reject(error);
            } else {
                reject("Error.");
                console.log(error);
            }
        });
    });
};
