// hammerbot: Dead Cells Discord bot
// Copyright (C) 2018 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Submitter = require('../../util/submitter.js');

module.exports = {
    trigger: 'suggestion',
    aliases: [
        {
            alias: 's',
            hidden: false
        }
    ],
    description: 'Submits a game-related suggestion to #suggestions.',
    params: [
        {
            name: 'text',
            type: 'clear',
            description: 'content of the submission',
            optional: false
        }
    ],
    dm: -1
}

module.exports.exec = (msg, context, params) => {
    Submitter(msg, params[0], 0, true);
}
