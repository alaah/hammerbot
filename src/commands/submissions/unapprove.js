// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Hammer = require('../../core/hammer.js');
const Discord = require('discord.js');
const Tools = require('../../util/tools.js');
const Server = require('../../core/server.js');
const Fetch = require('../../util/fetch.js');

module.exports = {
    trigger: 'unapprove',
    description: 'Marks a submission as unapproved.',
    params: [
        {
            name: 'id',
            type: 'int',
            description: 'id of the submission',
            optional: false
        }
    ],
    perm: 1,
    props: {
        clean: true
    }
}

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        Fetch(context, params.id).then(data => {
            if(data.row.type > 0) {
                reject('This submission is not approved.');
            } else {
                let embed = new Discord.RichEmbed(data.post.embeds[0]);
                embed.color = parseInt(context.server.get_channel(data.row.type).color1, 16);
                embed.footer.text = 'id: ' + data.id;
                embed.timestamp = new Date();
                resolve('Success.');
                data.post.edit(embed);
                const type = -data.row.type;
                Hammer.postgres.query(`update data set type=${type} where id=${data.id} and serverid='${context.server.id}';`, (err, res) => {
                    if(err) {
                        console.error(err);
                    }
                });
            }
        }).catch(reject);
    });
};
