// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const links = require('../../config/links.json');

module.exports = {
    trigger: 'links',
    aliases: [
        {
            alias: 'link',
            hidden: true
        }
    ],
    description: 'Prints useful game-related links.'
};

module.exports.exec = context => {
    return new Promise(resolve => {
        let embed = {};
        embed.title = 'Links'
        embed.color = 0xF4A460;
        embed.description = '';
        embed.fields = [];
        links.forEach(cat => {
            let desc = '';
            cat.links.forEach(link => {
                desc += '[' + link.title + '](<' + link.url + '>)\n';
            });
            embed.fields.push({
                name: cat.category,
                value: desc
            });
        });
        resolve({embed: embed});
    });
};
