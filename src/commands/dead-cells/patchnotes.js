// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Hammer = require('../../core/hammer.js');

module.exports = {
    trigger: 'patchnotes',
    aliases: [
        {
            alias: 'patch',
            hidden: false
        }
    ],
    description: 'Prints the latest Dead Cells patch notes'
}

module.exports.exec = context => {
    return new Promise((resolve, reject) => {
        let url_big = '<https://dead-cells.com/patchnotes/#v';
        let url_small = '<https://dead-cells.com/patchnotes/';
        const name = Hammer.client.channels.find(val => val.id === '521423577241747488').name;
        if(!name) {
            reject("Unable to find the patchnotes channel.")
        } else {
            let version_big = name[0] + name[1] + '.' + name[3];
            let version_small = name[0] + name[1];
            if(name[4] > '0' && name[4] <= '9') {
                version_big += name[4];
            }
            url_big += version_big + '>';
            url_small += version_small + '>';

            let embed = {};
            embed.title = 'Patchnotes'
            embed.color = 0x101524;
            embed.description = `[Recent patch (${version_big})](${url_big})\n`;
            embed.description += `[Update ${version_small}](${url_small})`;

            resolve({embed: embed});
        }
    });
}
