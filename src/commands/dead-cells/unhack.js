// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Discord = require('discord.js');
const links = require('../../config/links.json');

module.exports = {
    trigger: 'unhack',
    aliases: [
        {
            alias: 'hack',
            hidden: true
        },
        {
            alias: 'rehack',
            hidden: true
        }
    ],
    description: 'Haha no.',
    hidden: true
};

module.exports.exec = context => {
    return new Promise((resolve, reject) => {
        let rando = Math.round(Math.random() * 5) + +5;
        context.postgres.query(`update lead set score=score-${rando} where userid='${context.author.id}' and serverid='${context.guild.id}';`, (err, res) => {
            if(err) {
                console.log(err);
                reject("Fail.");
            }
        });
        context.channel.send('Oh no, I\'ve been hacked!').then(() => {
            setTimeout(() => {
                resolve('Looks like ' + context.author.username + ' has lost ' + rando + ' points! Whoops!');
            }, Math.random() * 5000);
        }).catch(console.error);
    });
};
