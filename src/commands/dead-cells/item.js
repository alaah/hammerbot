// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const GettextParser = require('gettext-parser');
const Fs = require('fs');
const LoadIcon = require('../../util/load-icon.js');
const Discord = require('discord.js');

let items;
let weapons;

(() => {
    const json = JSON.parse(Fs.readFileSync('files/cdb/data.cdb', {encoding: 'utf-8'}));
    const mo = GettextParser.mo.parse(Fs.readFileSync('files/cdb/main.en.mo')).translations[''];
    items = json.sheets[1].lines;
    weapons = json.sheets[3].lines;

    for(key in mo) {
        let new_key = key.replace(/[^\x00-\x7F]/g, '');
        if(key !== new_key) {
            Object.defineProperty(mo, new_key, Object.getOwnPropertyDescriptor(mo, key));
            delete mo[key];
        }
    }

    items.forEach(item => {
        if(mo[item.name.replace(/[^\x00-\x7F]/g, '')]) {
            item.name = mo[item.name.replace(/[^\x00-\x7F]/g, '')].msgstr[0];
        }
        if(mo[item.gameplayDesc]) {
            item.gameplayDesc = mo[item.gameplayDesc].msgstr[0];
        }
    });

    var results = [];
})();

const strToColor = str => {
    if(str === 'Tactic') {
        return 0xa051ff;
    }
    if(str === 'Survival') {
        return 0x17ce75;
    }
    if(str === 'Brutality'){
        return 0xec2424;
    }
    return 0x151515;
}

module.exports = {
    trigger: 'item',
    aliases: [
        {
            alias: 'i',
            hidden: false
        }
    ],
    params: [
        {
            name: 'item',
            type: 'clear',
            description: 'name of the item',
            optional: false
        },
    ],
    description: 'Prints basic information about an item.'
};

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        let item = null;
        let param = params.item.toLowerCase();
        items.forEach(found => {
            if(!item) {
                if(found.name.toLowerCase().indexOf(param) !== -1 ||
                   found.id.toLowerCase().indexOf(param) !== -1) {
                    item = found;
                }
            }
        });
        if(item) {
            let icon = LoadIcon.load(param);
            let embed = new Discord.RichEmbed();
            if(icon) {
                embed.attachFile({
                    attachment: 'files/gear/' + icon,
                    name: 'thumb.png'
                });
                embed.setThumbnail('attachment://thumb.png');
            }

            embed.setColor(strToColor(item.tier1));
            embed.setAuthor(item.name);
            embed.setTitle(item.tier1 + (item.tier2 ? (', ' + item.tier2) : ''));
            embed.setDescription(item.gameplayDesc);

            weapons.forEach(weapon => {
                if(weapon.item === item.id) {
                    let totalDamage = 0;
                    let totalDamageCrit = 0;
                    let totalDuration = 0;
                    weapon.strikeChain.forEach((strike, index) => {
                        let out = '';
                        out += 'Damage: ' + strike.power + '\n';
                        out += 'Charge point: ' + strike.charge + 's\n';
                        out += 'Control lock: ' + strike.lockCtrlAfter + 's\n';
                        if(strike.canCrit) {
                            out += 'Crit: ' + (strike.critMul * 200) + '%' + '\n';
                            totalDamageCrit += +strike.power * strike.critMul * 2;
                        }
                        out += 'Animation: ' + strike.animId + '\n';
                        embed.addField('Strike ' + (index + 1), out);
                        totalDamage += +strike.power;
                        totalDuration += +strike.charge + +strike.lockCtrlAfter;
                    });
                    let out = '';
                    out += 'Damage: ' + totalDamage + (totalDamageCrit ? (' (' + totalDamageCrit + ')') : '') +'\n';
                    out += 'Duration: ' + totalDuration.toPrecision(3) + 's\n';
                    out += 'DPS: ' + Math.round(totalDamage / totalDuration);
                    if(totalDamageCrit) {
                        out += ' (' + Math.round(totalDamageCrit / totalDuration) + ')';
                    }
                    embed.addField('Total', out)
                }
            });

            if(item.tags) {
                let tags = '';
                item.tags.forEach(tag => {
                    tags += tag.tag + ', ';
                });
                tags = tags.substr(0, tags.length - 2);
                if(tags) {
                    embed.addField('Tags', tags);
                }
            }

            if(item.props) {
                let props = '';
                for(let prop in item.props) {
                    props += prop + ': ' + item.props[prop] + '\n';
                }
                if(props) {
                    embed.addField('Props', props, true);
                }
            }

            embed.addField('Cost', 'Money: ' + item.moneyCost + '\nCells: ' + item.cellCost, true);
            embed.addField('Codename', item.id, true);
            resolve(embed);
        } else {
            reject(params.item + ' not found.');
        }
    });
};
