// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const LoadIcon = require('../../util/load-icon.js');

module.exports = {
    trigger: 'icon',
    aliases: [
        {
            alias: 'give',
            hidden: false
        },
        {
            alias: 'drop',
            hidden: false
        }
    ],
    params: [
        {
            name: 'item',
            type: 'word',
            description: 'name of the item with whitespace removed or replaced with _',
            optional: false
        }
    ],
    description: 'Uploads an icon of an item.',
    detail: 'Use !listicons to see all possible icons.'
};

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        let file = LoadIcon.load(params.item);
        if(file) {
            resolve({
                files: [{
                    attachment: 'files/gear/' + file,
                    name: 'files/gear/' + file
                }]
            }).catch(console.error);
        } else {
            reject('Unknown icon.');
        }
    });
};
