// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const source = require('../../config/trivias.json');
const Tools = require('../../util/tools.js');
let trivia_channels = new Map;

module.exports = {
    trigger: 'trivia',
    description: 'Prints a random interesting fact about Dead Eels.',
    params: [
        {
            name: 'substring',
            type: 'clear',
            description: 'chunk of text to look for',
            optional: true
        }
    ]
};

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        const cooldown = 5 * 1000; // 5s
        const reset_time = 120 * 60 * 1000; // 120min
        const limit = 75;

        let off_cooldown = true;
        if(!trivia_channels.has(context.channel.id) || trivia_channels.get(context.channel.id).length == 0) {
            trivia_channels.set(context.channel.id, {data: Array.from(Array(source.length).keys()), time: new Date(), count: 0});
        } else {
            off_cooldown = new Date() - trivia_channels.get(context.channel.id).time > cooldown;
        }

        let count = trivia_channels.get(context.channel.id).count;
        if(context.channel.type === 'dm' && count >= limit && new Date() - trivia_channels.get(context.channel.id).time > reset_time) {
            trivia_channels.get(context.channel.id).count = 0;
        }
        if(context.channel.type !== 'dm' || count < limit) {
            if(off_cooldown) {
                ++trivia_channels.get(context.channel.id).count;
                trivia_channels.get(context.channel.id).time = new Date();
                let local_array = trivia_channels.get(context.channel.id).data;
                let random = Tools.arrayPick(local_array);
                if(params.substring) {
                    let searched = [];
                    local_array.forEach(elem => {
                        let item = source[elem];
                        if(item.text && item.text.toLowerCase().indexOf(params.substring.toLowerCase()) !== -1) {
                            searched.push(elem);
                        }
                    });
                    if(searched.length > 0) {
                        random = Tools.arrayPick(searched);
                    }
                }
                let item = source[random];
                for(let i = 0; i < local_array.length; ++i) {
                    if(local_array[i] == random) {
                        local_array.splice(i, 1);
                    }
                }

                let data = item.text ? item.text : '';
                if(item.quote) {
                    data += '\n*~' + item.quote + '*';
                }
                if(item.author) {
                    data += '\n*submitted by ' + item.author + '*';
                }
                if(item.link) {
                    data += '\n' + item.link;
                }

                resolve(data);
            } else {
                reject('Trivia has a cooldown of 5s.');
            }
        } else if(count === limit) {
            ++trivia_channels.get(context.channel.id).count;
            reject('Whoa, slow down. You\'re dangerously curious.');
        }
    });
}
