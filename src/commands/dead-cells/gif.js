// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Fs = require('fs');
const Server = require('../../core/server.js');

let gifs = [];

Fs.readdir('files/emoji', (err, files) => {
    if(err) {
        throw err;
    } else {
        files.forEach(file => {
            gifs.push(file.substr(0, file.length - 4));
        });
    }
});

module.exports = {
    trigger: 'gif',
    aliases: [
        {
            alias: 'giff',
            hidden: false
        },
        {
            alias: 'emoji',
            hidden: false
        },
        {
            alias: 'emote',
            hidden: false
        }
    ],
    params: [
        {
            name: 'gif',
            type: 'word',
            description: 'name of the gif; see `giflist`',
            optional: false
        },
        {
            name: 'message',
            type: 'clear',
            description: 'content of your message',
            optional: true
        }
    ],
    description: 'Adds an animated gif of the Beheaded to your message.',
    custom_gif_category: true,
    props: {
        delete_parent: true
    }
};

// Used by giflist
module.exports.gifs = gifs;

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        let found = false;
        gifs.forEach(gif => {
            if(!found && gif.toLowerCase().indexOf(params.gif) !== -1) {
                if(context.channel.type === 'dm') {
                    context.channel.send({
                        files: [{
                            attachment: 'files/emoji/' + gif + '.gif',
                            name: gif + '.gif'
                        }]
                    }).catch(console.error);
                } else {
                    context.channel.createWebhook(context.member.displayName, context.author.avatarURL)
                    .then(wb => {
                        context.channel.send(params.message, {
                            files: [{
                                attachment: 'files/emoji/' + gif + '.gif',
                                name: gif + '.gif'
                            }]
                        }).then(() => {
                            wb.delete().then(resolve).catch(console.error);
                        }).catch(console.error);
                    }).catch(console.error);
                }
                found = true;
            }
        });
        if(!found) {
            reject(`Gif not found. See \`${Server.prefix(context)}giflist\``);
        }
    });
};
