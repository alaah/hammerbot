// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Parser = require('../../core/command-parser.js');
const Tools = require('../../util/tools.js');
const Categories = require('../categories.json');

let usageOf = command => {
    let usage = '';
    if(command.params) {
        command.params.forEach(param => {
            let brackets = param.optional ? '[]' : '<>';
            usage += brackets[0] + param.name + brackets[1] + ' ';
        });
    }
    return usage;
}

module.exports = {
    trigger: 'help',
    aliases: [
        {
            alias: '?',
            hidden: false
        },
        {
            alias: 'h',
            hidden: false
        }
    ],
    description: 'Prints details about a category or command.',
    params: [
        {
            name: 'category_or_command',
            type: 'word',
            optional: true
        }
    ],
    props: {
        dm_out: true
    }
};

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        let embed = {
            color: 0x9952ff,
            title: 'Usage',
            fields: []
        };
        if(!params.category_or_command) {
            embed.description = 'help [category/command]';
            Categories.forEach((category) => {
                if(category.name !== 'hidden') {
                    if(!context.server.flags.submit && category.name === 'submissions') {}
                    else if(!context.server.flags.rps && category.name === 'minigame') {}
                    else if(!context.server.flags.dc && category.name === 'dead-cells') {}
                    else {
                        embed.fields.push({
                            name: Tools.markAlias(category.name, category.alias),
                            value: category.description
                        });
                        is_category = true;
                    }
                }
            });
            resolve({
                embed: embed
            });
        } else {
            embed.description = '@Hammer command [arguments] or (DM only) command [arguments]';
            let fields = [];
            let is_category = false;
            let cat = params.category_or_command;
            Categories.forEach(category => {
                if(cat === category.alias) {
                    cat = category.name;
                }
                if(cat === category.name) {
                    is_category = true;
                }
            });
            if(is_category) {
                Parser.commands.forEach(command => {
                    if(command.category === cat) {
                        let trigger = command.trigger;
                        let aliases = '';
                        if(command.aliases) {
                            command.aliases.forEach(alias => {
                                if(!alias.hidden) {
                                    let pos = command.trigger.indexOf(alias.alias);
                                    if(pos === -1 || alias.alias.length > 1) {
                                        if(aliases === '') {
                                            aliases = '(alias '
                                        }
                                        aliases += alias.alias + ', ';
                                    } else {
                                        trigger = Tools.markAlias(command.trigger, alias.alias);
                                    }
                                }
                            });
                            if(aliases !== '') {
                                aliases = aliases.substr(0, aliases.length - 2);
                                aliases += ')';
                            }
                        }

                        fields.push([trigger + ' ' + usageOf(command) + aliases, command.description]);
                    }
                });
                fields.sort((a, b) => {
                    return a[0] < b[0] ? -1 : 1;
                });
                fields.forEach(field => {
                    embed.fields.push({
                        name: field[0],
                        value: field[1]
                    });
                });
            } else {
                let command = Parser.findCommand(params[0], true);
                if(command) {
                    embed.description = command.trigger + ' ' + usageOf(command);
                    embed.fields.push({
                        name: 'Description',
                        value: command.description + (command.detail ? (' ' + command.detail) : '')
                    });
                    if(command.custom_fields) {
                        command.custom_fields.forEach(field => {
                            embed.fields.push({
                                name: field.field,
                                value: field.text
                            });
                        });
                    }
                    if(command.params) {
                        let arguments = '';
                        command.params.forEach(param => {
                            arguments += '**' + param.name + '**: ' + param.description + '\n';
                        });
                        embed.fields.push({
                            name: 'Arguments',
                            value: arguments
                        });
                    }
                    if(command.aliases) {
                        let aliases = '';
                        command.aliases.forEach(alias => {
                            aliases += alias.alias + ', ';
                        });
                        aliases = aliases.substr(0, aliases.length - 2);
                        embed.fields.push({
                            name: 'Aliases',
                            value: aliases
                        });
                    }
                    let other = '';
                    if(command.perm) {
                        if(command.perm === 1) {
                            other += 'Requires privileges.\n';
                        } else if(command.perm === 2) {
                            other += 'Owner only.\n';
                        }
                    }
                    if(command.dm) {
                        if(command.dm === 1) {
                            other += 'DM only.\n';
                        } else if(command.dm === -1) {
                            other += 'Public channels only.\n';
                        }
                    }
                    if(other) {
                        embed.fields.push({
                            name: 'Other',
                            value: other
                        });
                    }
                }
            }
            if(embed.fields.length > 0) {
                resolve({
                    embed: embed
                });
            } else {
                reject(`Unknown term ${params[0]}.`);
            }
        }
    });
}
