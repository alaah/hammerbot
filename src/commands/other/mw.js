// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const MW = require('mw-dict');
const Tokens = require('../../config/tokens.json');
const Permissions = require('../../util/permissions.js');
const Discord = require('discord.js');

const dict = new MW.CollegiateDictionary(Tokens.dictionary);

module.exports = {
    trigger: 'mw',
    description: 'Prints a definition from Merriam-Webster.',
    params: [
        {
            name: 'word',
            type: 'clear',
            description: 'term to look for; can be multi-word',
            optional: false
        }
    ],
    dm: -1
}

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        dict.lookup(params.word).then(result => {
            let definition = result[0];
            let embed = new Discord.RichEmbed;
            embed.setTitle('Definition of ' + definition.word);
            let description = '';
            let i = 1;
            definition.definition.forEach(sense => {
                if(sense.senses != null && sense.senses.length > 0) {
                    let j = 'a';
                    sense.senses.forEach((subsense) => {
                        description += i + j + ': ';
                        if(subsense.synonyms != null) {
                            description += subsense.synonyms.toString().toUpperCase();
                        }
                        if(subsense.meanings != null) {
                            description += subsense.meanings.toString().substr(2);
                        }
                        description += '\n';
                        j = String.fromCharCode(j.charCodeAt() + 1);
                    });
                } else {
                    description += i + sense.meanings.toString() + '\n';
                }
                ++i;
            });
            embed.setColor('2d5f7c');
            embed.setDescription(description);
            embed.setFooter('Definition from Merriam-Webster');
            resolve(embed);
        }).catch(error => {
            if(error instanceof MW.WordNotFoundError) {
                resolve({
                    output: `Word ${params.word} not found on Merriam-Webster.`,
                    failed: true
                });
            } else {
                reject(error);
            }
        });
    });
};
