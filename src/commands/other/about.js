// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Tools = require('../../util/tools.js');
const links = require('../../config/links.json');

module.exports = {
    trigger: 'about',
    description: 'Prints information about Hammer.',
    props: {
        dm_out: true
    }
};

module.exports.exec = () => {
    return new Promise((resolve, reject) => {
        let footers = [
            'Why is it Hammer and not a more suitable entity like the Scribe, you may ask. The answer is: Hammers deserve love; they are the rarest mobs in the game.',
            'Hammer does not like Sickle.',
            'JS really sucks.',
            'Formerly Known As Mechanical Spider',
            'This is a footer.',
            'The Nothing-Solving Hammer',
            'The only good video: youtube.com/embed/kV7jjdxAWqA'
        ];
        const embed = {
            color: Math.random() < 0.5 ? 0x1c2d71 : 0x7a1461,
            description: 'Hammer /ˈhæm.ə(ɹ)/ – mob that can only be found in the ' + (Math.random() < 0.1 ? '🅱' : 'P') + 'rison Depths. Capable of creating minefields and spawning hordes of annoying biters, he is the best spider. Back in the good times, he used to have a spikey slam attack.',
            fields: [
                {
                    name: 'Author',
                    value: 'alaah#2380'
                },
                {
                    name: 'Version',
                    value: '5.0.1 (2019-03)'
                },
                {
                    name: 'Patchnotes',
                    value: '**5.0.1:**\n\
+Categorized !links\n\
\n\
**5.0:**\n\
+0-5bc commands.\n\
+Custom submission types.\n\
*Updated Dead Cells data to 1.2.\n\
*Reworked !patchnotes.\n\
*Rewritten all commands in a streamlined Promise-based system.\n\
*Hammer no longer uses legendaries if you don\'t choose colorless.\n\
*Patreon link now actually works in case someone has too much money.'
                },
                {
                    name: 'Useful links',
                    value: '[Source code](https://gitlab.com/alaah/hammerbot)\n[YouTube](https://www.youtube.com/channel/UCRf1ldnsJXYuy5KV4qzmATg)\n[Patreon](https://www.patreon.com/alaah)' + (Math.random() < 0.3 ? '\n[Cute Shantae](https://orig00.deviantart.net/720a/f/2016/172/7/9/shantae____by_zedrin-da75hb6.png)' : '')
                }
            ],
            footer: Math.random() < 0.6 ? Tools.arrayPick(footers) : null
        };
        resolve({embed: embed});
    });
};
