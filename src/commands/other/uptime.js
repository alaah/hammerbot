// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const time_start = new Date();

module.exports = {
    trigger: 'uptime',
    description: 'Prints how long Hammer\'s been running. He automatically resets from time to time.'
}

module.exports.exec = () => {
    return new Promise((resolve, reject) => {
        const time = Math.round((new Date() - time_start) / 1000.);
        resolve('I have been online for ' + Math.floor(time / 3600) + ' hours, ' + Math.floor((time % 3600) / 60) + ' minutes and ' +  (time % 60) + ' seconds.');
    });
}
