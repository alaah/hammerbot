// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Server = require('../../core/server.js');
const Hammer = require('../../core/hammer.js');

module.exports = {
    trigger: 'react',
    params: [
        {
            name: 'emoji',
            type: 'word',
            description: 'name of the emoji without colons',
            optional: false
        }
    ],
    description: 'Reacts to the message above.',
    props: {
        delete_parent: true
    }
};

let reactions = new Map();

Hammer.client.on('messageReactionAdd', (reaction, user) => {
    if(user !== Hammer.self) {
        let key = `${reaction.message.id}+${reaction.emoji.id}`;
        let found = reactions.get(key);
        if(found) {
            found.remove();
            reactions.delete(key);
        }
    }
});

module.exports.exec = (context, params) => {
    return new Promise((resolve, reject) => {
        let emoji = context.guild.emojis.find(val => val.name.toLowerCase() === params.emoji);
        if(emoji) {
            context.channel.fetchMessages({limit: 1}).then(messages => {
                let target = messages.first();
                target.react(emoji).then(react => {
                    reactions.set(`${target.id}+${emoji.id}`, react);
                    resolve();
                }).catch(console.error);
            }).catch(console.error);
        } else {
            reject("Unknown emoji.");
        }
    });
};
