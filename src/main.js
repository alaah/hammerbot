// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Tokens = require('./config/tokens.json');
const Hammer = require('./core/hammer.js');
const CommandParser = require('./core/command-parser.js');
const RegexParser = require('./core/regex-parser.js');
const SetupManager = require('./core/setup-manager.js');
const Tools = require('./util/tools.js');
const Server = require('./core/server.js')

Hammer.client.on('ready', () => {
    let activities = [
        ['your ideas', 'LISTENING'],
        ['complaints', 'LISTENING'],
        ['Dead Eels', 'PLAYING'],
        ['Hello Night', 'PLAYING'],
        ['Deceased Microorganisms', 'PLAYING'],
        ['Dankest Gungeon', 'PLAYING'],
        ['mw deprecated', 'PLAYING'],
        ['you', 'WATCHING']
    ];
    if(Math.random() < 0.5) {
        let activity = Tools.arrayPick(activities);
        Hammer.client.user.setActivity(activity[0], {type: activity[1]});
    }
    Hammer.init();
    Server.init();
    console.log('Ready.');
});

Hammer.client.on('message', msg => {
    if(msg.author !== Hammer.self && !msg.content.startsWith('Unknown')) {
        if(SetupManager.isOn(msg.channel)) {
            SetupManager.query(msg);
        } else {
            if(Server.from(msg).flags.regex) {
                RegexParser.parse(msg).then(not_complain => {
                    CommandParser.parse(msg, not_complain);
                });
            } else {
                CommandParser.parse(msg, false);
            }
        }
    }
});

Hammer.client.on("guildCreate", guild => {
    console.log("Joined a guild: " + guild.name);
    Server.push(guild.id);
})

Hammer.client.on("guildDelete", guild => {
    console.log("Left a guild: " + guild.name);
    Server.pop(guild.id);
})

Hammer.client.on('error', console.error);

Hammer.client.login(Tokens.discord);
