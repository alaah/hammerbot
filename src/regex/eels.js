// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

module.exports.regex = [
    /\bdea?d eels\b/mi
];

module.exports.mention_only = false;

module.exports.probability = 0.33;
module.exports.exec_chance = msg => {
    msg.channel.send({
        files: [{
            attachment: 'files/regex/eels.png',
            name: 'eels.png'
        }]
    }).catch(console.error);
};
