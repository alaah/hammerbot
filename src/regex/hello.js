// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Hammer = require('../core/hammer.js');
const Settings = require('../config/settings.json');

module.exports.regex = [
    /\b(bonjour|salut|hello|hi|greetings|bye|see (you|ya)|cya|goodnight|(good )?morning|(good )?afternoon|(good )?evening|dzień dobry|dobranoc|guten morgen|guten tag|guten abend|buon ?giorno)\b/mi
];
module.exports.mention_only = true;
module.exports.exec_always = msg => {
    msg.react('👋');
};
