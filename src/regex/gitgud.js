// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

module.exports.regex = [
    /\bi\b(need|want|request)\b.*\b(help|assist) me\b/mi,
    /\bhow (do|can|shall|should) (i|you)\b.*\b(beat|win|fight|kill|flawless|nohit)\b/mi,
    /\bis there\b.*\bway\b.*\b(beat|win|fight against|kill|flawless|nohit)\b/mi,
    /\bi\b.*\bdied? (to|from)\b.*\b(assassin|ass|hotk|hand|ico|concierge|incomplete|hammer|inquisitor|mage|watcher|conjo?u?n?ctivit?i?u?o?s|spinner|lancer|kamikaze|bat)\b/mi,
    /\bi suck at\b....*/mi,
    /\bi'?m bad\b/mi,
    /\bis\b.*\b(hard|difficult|ridiculous) (to me|for me|imh?o)\b/mi,
    /\b(assassin|ass|hotk|hand|ico|concierge|incomplete|hammer|inquisitor|mage|watcher|conjo?u?n?ctivit?i?u?o?s|spinner|lancer|kamikaze|bat)\b.*\breee*\b/mi,
    /\bi\b.*\b(hate|despise)\b.*\b(assassin|ass|hotk|hand|ico|concierge|incomplete|hammer|inquisitor|mage|watcher|conjo?u?n?ctivit?i?u?o?s|spinner|lancer|kamikaze|bat)\b/mi,
    /\b(screw|freaking|(fuck|fook)(ing)?)\b.*\b(monikammm*|assassin|ass|hotk|hand|ico|concierge|incomplete|hammer|inquisitor|mage|watcher|conjo?u?n?ctivit?i?u?o?s|spinner|lancer|kamikaze|bat)\b/mi
];

module.exports.probability = 0.33;
module.exports.exec_chance = msg => {
    let curr = new Date();
    let file = null;

    let generic_files = [
        'hornet.png',
        'spook.png',
        'souls.png'
    ];

    if(/(inquisitor|mage)/mg.test(msg)) {
        file = 'mage.png';
    } else if(/(hotk\b|\bhand\b)/mg.test(msg)) {
        file = 'tears.png';
    } else {
        file = generic_files[Math.floor(Math.random() * generic_files.length)];
    }

    msg.channel.send({
        files: [{
            attachment: 'files/regex/' + file,
            name: 'files/regex/' + file
        }]
    }).catch(console.error);
}
