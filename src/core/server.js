// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Discord = require('discord.js');
const Hammer = require('./hammer.js');
const Categories = require('../commands/categories.json');
const Settings = require('../config/settings.json');
const Tools = require('../util/tools.js');

class Server {
    constructor(row) {
        this.init(row);
    }

    init(row) {
        this.id = row.id;
        this.ready = row.ready;
        this.reload_channels();
        this.emojis = {
            upboat: Hammer.client.guilds.get(this.id).emojis.get(row.upboat),
            downboat: Hammer.client.guilds.get(this.id).emojis.get(row.downboat)
        };
        this.flags = row.flags ? {
            dc: row.flags & 1,
            rps: row.flags & 2,
            gif: row.flags & 4,
            submit: row.flags & 8,
            regex: row.flags & 16
        } : {
            dc: 0,
            rps: 0,
            gif: 0,
            submit: 0,
            regex: 0
        };
        this.prefix = row.prefix;
        console.log(`Joined ${Hammer.client.guilds.get(this.id).name} (${this.id}) with flags ${row.flags}.`);
    }

    get_channel(id) {
        let result = null;
        this.channels.forEach(channel => {
            if(channel.id === id || channel.id === -id) {
                result = channel;
            }
        });
        return result;
    }

    reload_channels() {
        this.channels = [];
        Hammer.postgres.query(`select * from channels where serverid='${this.id}';`, (err, res) => {
            if(res && res.rows) {
                res.rows.forEach(row => {
                    this.channels.push({
                        id: row.localid,
                        name: row.name,
                        channel: Hammer.client.channels.get(row.channelid),
                        color1: row.color1,
                        color2: row.color2,
                        triggers: row.triggers,
                        is_suggestion: row.is_suggestion
                    });
                });
                if(err) {
                    console.error(err);
                }
            };
        });
    }

    check(command) {
        if(command.category === 'minigame' && !this.flags.rps) {
            return false;
        }
        if(command.category === 'dead-cells' && !command.trigger.startsWith('gif') && !this.flags.dc) {
            return false;
        }
        if(command.category === 'dead-cells' && command.trigger.startsWith('gif') && !this.flags.gif) {
            return false;
        }
        if(command.category === 'submissions' && !this.flags.submit) {
            return false;
        }
        return true;
    };

    get downboat() {
        return this.emojis.downboat ? this.emojis.downboat : home.downboat;
    }

    get upboat() {
        return this.emojis.upboat ? this.emojis.upboat : home.upboat;
    }
}

let home = null;
let servers = new Map;

module.exports.init = () => {
        Hammer.postgres.query('select * from servers;', (err, res) => {
        res.rows.forEach(row => {
            servers.set(row.id, new Server(row));
        });
        home = servers.get(Settings.home);
        if(err) {
            console.error(err);
        }
    });
};

module.exports.push = id => {
    Hammer.postgres.query('insert into servers(id, ready) values(\'' + id + '\', false)', (err, res) => {
        if(err) {
            console.error(err);
        }
    });
    servers.set(id, new Server({
        id: id,
        ready: false
    }));
};

module.exports.pop = id => {
    Hammer.postgres.query('delete from servers where id=\'' + id + '\'', (err, res) => {
        if(err) {
            console.error(err);
        }
    });
    servers.delete(id);
};

module.exports.home = msg => {
    return home;
};

module.exports.from = msg => {
    return msg.channel.type === 'dm' ? home : servers.get(msg.guild.id);
};

module.exports.byId = id => {
    return servers.get(id);
};

module.exports.prefix = msg => {
    return msg.channel.type === 'dm' ? '' : servers.get(msg.guild.id).prefix ? servers.get(msg.guild.id).prefix : '@Hammer ';
};
