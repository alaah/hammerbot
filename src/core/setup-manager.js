// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Hammer = require('./hammer.js');
const Server = require('./server.js');

instances = [];

const questions = [
    /* 0 */ "Warning: this server is already configured. Do you want to continue? (y/N)",
    /* 1 */ "Do you want to have submission channels? (y/N)",
    /* 4 */ "Do you want to have Dead Cells-related commands? (y/N)",
    /* 5 */ "Do you want to have the rock-paper-scissors minigame? (y/N)",
    /* 6 */ "Do you want to have the `gif` command? (y/N)\n\
Please note it sort of spams the audit log when used.",
    /* 7 */ "Do you want Hammer to react to regex patterns like thanks hammer? (y/N)\n\
This will sometimes embed a picture when triggered.",
    /* 8 */ "Finally, type the command prefix."
];

class Instance {
    constructor(guild, channel) {
        this.done = false;
        this.guild = guild;
        this.channel = channel;
        this.submission_channels = [];
        this.stage = 0;
        this.prefix = '!';
        this.flags = 0;
    }

    parseStage() {
        return new Promise((resolve, reject) => {
            Hammer.postgres.query(`select ready from servers where id='${this.guild.id}'`, (err, res) => {
                if(err) {
                    reject(err);
                } else {
                    this.stage = res.rows[0].ready ? 0 : 1;
                    resolve();
                }
            });
        });
    }

    write() {
        this.channel.send(questions[this.stage]);
    }

    finalize() {
        let query = `update servers set \
        ready=true, \
        flags=${this.flags}, \
        prefix='${this.prefix}'`;
        if(this.submission_channels.length > 0) {
            query += ', channels={';
            this.submission_channels.forEach(chan => {
                query += `'${chan.id}',`;
            });
            query = query.substr(0, query.length - 1);
            query += '}'
        }
        query += `where id='${this.guild.id}'`;
        Hammer.postgres.query(query, (err, res) => {
            Hammer.postgres.query(`select * from servers where id='${this.guild.id}'`, (err, res2) => {
                res2.rows.forEach(row => {
                    Server.byId(this.guild.id).init(row);
                });
                if(err) {
                    console.error(err);
                }
            });
        });
    }

    pass(text) {
        let yes = text.charAt(0) === 'y';

        const fin = () => {
            this.done = true;
            this.channel.send('Setup over. Thanks.');
        };

        if(text === 'cancel' || !this.guild.available) {
            fin();
        } else {
            let chan = null;
            switch(this.stage) {
                case 0:
                    if(yes) {
                        this.stage = 1;
                    } else {
                        this.stage = 0;
                        fin();
                    }
                    break;
                case 1:
                    if(yes) {
                        this.flags += 8;
                        this.channel.send('Add channels using !set_channel <channelid> <hex_color> <hex_color_when_approved> <submission_name>, e.g.:\n\
!set_channel 521430120666497026 #DD0050 #5000DD Bug Report\n\
This setting is permanent.');
                    }
                    this.stage = 4;
                    break;
                case 4:
                    if(yes) {
                        this.stage = 6;
                        this.flags += 3;
                    } else {
                        this.stage = 5;
                    }
                    break;
                case 5:
                    if(yes) {
                        this.stage = 6;
                        this.flags += 2;
                    } else {
                        this.stage = 6;
                    }
                    break;
                case 6:
                    if(yes) {
                        this.flags += 4;
                        this.stage = 7;
                    } else {
                        this.stage = 7;
                    }
                    break;
                case 7:
                    if(yes) {
                        this.flags += 16;
                        this.stage = 8;
                    } else {
                        this.stage = 8;
                    }
                    break;
                case 8:
                    this.prefix = text;
                    this.finalize();
                    fin();
                    break;
            }
            if(!this.done) {
                this.write();
            }
        }
    }
};

module.exports.query = msg => {
    let found = false;
    instances.forEach((instance, index) => {
        if(instance.done) {
            instances.splice(index, 1);
        } else if(!found && instance.channel.id === msg.channel.id) {
            instance.pass(msg.content.toLowerCase());
            found = true;
        }
    });
    return found;
};

module.exports.isOn = channel => {
    let found = false;
    if(channel.type !== 'dm') {
        return false;
    }
    instances.forEach((instance, index) => {
        if(!found && instance.channel.id === channel.id) {
            found = true;
        }
    });
    return found;
};

module.exports.begin = (guild, channel) => {
    channel.send('Type `cancel` if you want to end (without saving changes).');
    let instance = new Instance(guild, channel);
    instance.parseStage().then(() => {
        instance.write();
    }).catch(err => {
        console.error(err);
    });
    instances.push(instance);
};
