// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Discord = require('discord.js');
const Hammer = require('./hammer.js');
const Categories = require('../commands/categories.json');
const Settings = require('../config/settings.json');
const Tools = require('../util/tools.js');
const Permissions = require('../util/permissions.js');
const Server = require('./server.js');
const Submitter = require('../util/submitter.js');

let commands = [];

let testTriggers = (command, trigger, trigger_shortcut) => {
    let found = false;
    if(command.trigger === trigger) {
        found = true;
    } else if(command.aliases) {
        command.aliases.forEach(alias => {
            if(alias.alias === trigger) {
                if(alias.alias.length > 1 || trigger_shortcut) {
                    found = true;
                }
            }
        });
    }
    return found;
};

let tryPrefix = (split, props, prefix) => {
    if(split[0].length > 1 && split[0].startsWith(prefix)) {
        split[0] = split[0].substr(prefix.length);
        return true;
    }
    return false;
};

let tryMention = (split, props) => {
    let index = split.indexOf(Hammer.self.toString());
    if(index !== -1) {
        split.splice(index, 1);
        if(index > 0) {
            props.complain_if_incorrect = false;
            props.can_use_clear = false;
            props.trigger_shortcut = false;
        }
        return true;
    }
    return false;
};

let tryName = (split, props) => {
    let index = split.indexOf(Settings.name);
    if(index !== -1) {
        split.splice(index, 1);
        props.complain_if_incorrect = false;
        props.can_use_clear = false;
        props.trigger_shortcut = false;
        return true;
    }
    return false;
};

const context_from_msg = msg => {
    return {
        author: msg.author,
        member: msg.member,
        guild: msg.guild,
        channel: msg.channel,
        server: Server.from(msg)
    };
}

module.exports.commands = commands;

module.exports.findCommand = (trigger, trigger_shortcut) => {
    for(let i = 0; i < commands.length; ++i) {
        let command = commands[i];
        if(command.trigger === trigger) {
            return command;
        } else if(command.aliases) {
            for(let i = 0; i < command.aliases.length; ++i) {
                let alias = command.aliases[i].alias;
                if(alias === trigger) {
                    if(alias.length > 1 || trigger_shortcut) {
                        return command;
                    }
                }
            };
        }
    }
    return null;
};

module.exports.exec = (msg, trigger, params) => {
    return new Promise((resolve, reject) => {
        let found = false;
        for(let i = 0; i < commands.length; ++i) {
            if(commands[i].trigger === trigger) {
                found = true;
                if(!params) {
                    params = [];
                }
                launch(msg, commands[i], params).then(resolve).catch(reject);
            }
        }
        if(!found) {
            reject();
        }
    });
};

const prelaunch = (msg, command) => {
    return new Promise((resolve, reject) => {
        if(command.props && command.props.delete_parent) {
            msg.delete().then(resolve).catch(reject);
        } else {
            resolve();
        }
    });
}

const launch = (msg, command, params) => {
    return new Promise((resolve, reject) => {
        prelaunch(msg, command).then(() => {
            console.log('Executing', command.trigger, params, 'in', msg.channel.type === 'dm' ? 'DM' : msg.channel.name, 'at', msg.channel.type === 'dm' ? 'DM' : msg.guild.name, 'by', msg.author.tag);
            command.exec(context_from_msg(msg), params).then(results => {
                if(results && (typeof results === 'string' || results.embed || results.files || results instanceof Discord.RichEmbed)) {
                    let printed_msg;
                    if(command.props && command.props.dm_out) {
                        msg.author.send(results).then(sent => {
                            printed_msg = sent;
                            resolve();
                        }).catch(reject);
                    } else {
                        msg.channel.send(results).then(sent => {
                            printed_msg = sent;
                            resolve();
                        }).catch(reject);
                    }
                    if(command.props && command.props.clean) {
                        setTimeout(() => {
                            if(printed_msg) {
                                printed_msg.delete();
                            }
                        }, 5000);
                    }
                } else {
                    resolve();
                }
            }).catch(reject);
        }).catch(reject);
    });
}

const process = (msg, channel, split, props) => {
    return new Promise((resolve, reject) => {
        let incorrect = false;
        let found = false;
        commands.forEach(command => {
            if(!found && testTriggers(command, split[0], props.trigger_shortcut)) {
                let params = {};
                let error = '';

                if(msg.channel.type !== 'dm') {
                    if(!Server.from(msg).check(command)) {
                        incorrect = true;
                        resolve(`Command ${split[0]} is disabled on this server.`);
                    }
                }
                if(command.dm) {
                    if(command.dm === -1 && msg.channel.type === 'dm') {
                        incorrect = true;
                        resolve(`Command ${split[0]} cannot be used in DM.`);
                    } else if(command.dm === 1 && msg.channel.type !== 'dm') {
                        incorrect = true;
                        resolve(`Command ${split[0]} must be used in DM.`);
                    }
                }
                if(command.perm) {
                    if((command.perm === 1 && !Permissions.isPrivileged(msg))
                    || (command.perm === 2 && !Permissions.isMaster(msg.author))) {
                        incorrect = true;
                        resolve('Insufficient privileges.');
                    }
                }
                if(command.params && split.length > 1) {
                    let start = 0;

                    while(command.params.length > split.length - 1 + start) {
                        if(command.params[start].optional) {
                            ++start;
                        } else {
                            break;
                        }
                    }

                    let position = msg.content.toLowerCase().indexOf(split[1]);

                    let cleared = false;
                    command.params.forEach((param, index) => {
                        if(!cleared && index + 1 - start < split.length) {
                            if(param.type === 'int' && isNaN(split[index + 1 - start])) {
                                incorrect = true;
                                resolve(`Argument ${param.name} must be a number.`);
                            }
                            if(param.type !== 'clear') {
                                params[param.name] = split[index + 1 - start];
                                position += split[index + 1 - start].length + 1;
                            } else {
                                if(props.can_use_clear) {
                                    params[param.name] = msg.content.substr(position);
                                    cleared = true;
                                } else {
                                    incorrect = true;
                                    resolve();
                                }
                            }
                        } else if(!param.optional) {
                            incorrect = true;
                            resolve(`Invalid arguments. Type \`${Server.prefix(msg)}help ${split[0]}\`.`);
                        }
                    });
                }
                found = true;
                if(!incorrect) {
                    launch(msg, command, params).then(resolve).catch(reject);
                }
            }
        });
        // Check for server-specific submission commands
        if(!found && msg.channel.type !== 'dm') {
            Server.from(msg).channels.forEach(data => {
                if(data.triggers) {
                    data.triggers.forEach(trigger => {
                        console.log(trigger);
                        if(split[0] === trigger) {
                            found = true;
                            Submitter(msg, context_from_msg(msg), msg.content.substr(msg.content.toLowerCase().indexOf(split[1])), data).then(resolve).catch(reject);
                        }
                    });
                }
            });
        }
        if(!found) {
            resolve(`Unknown ${split[0].length > 1 ? 'command' : 'alias'} ${split[0]}. Type \`${Server.prefix(msg)}help\` for the list of commands.`);
        }
    });
}

module.exports.parse = (msg, forcefully_not_complain) => {
    let split = msg.content.replace(/(@!)/gi, '@') || '';
    split = (split.replace(/[\.,;]/gi, '').toLowerCase() || '').split(/\s+/gi);
    let props = {
        complain_if_incorrect: !forcefully_not_complain,
        can_use_clear: true,
        trigger_shortcut: true,
    }
    let correct = false;
    if(msg.channel.type !== 'dm') {
        correct = tryPrefix(split, props, Server.prefix(msg));
    }
    if(!correct) {
        correct = tryMention(split, props);
    }
    if(!correct) {
        correct = tryName(split, props);
    }
    if(!correct && msg.channel.type === 'dm') {
        correct = true;
    }

    if(correct) {
        let channel = msg.channel;
        process(msg, channel, split, props).then(complaint => {
            if(complaint && typeof(complaint) === 'string' && props.complain_if_incorrect) {
                channel.send(complaint).then(unknown => {
                    setTimeout(() => {
                        unknown.delete();
                    }, 5000);
                });
            }
        }).catch(error => {
            if(error && typeof(error) === 'string') {
                channel.send(error).then(unknown => {
                    setTimeout(() => {
                        unknown.delete();
                    }, 5000);
                });
            } else if(error) {
                console.error('Error:');
                console.error(error);
                console.trace();
            } else {
                console.error('Rejection without a message.');
                console.trace();
            }
        });
    }
}

let loadCategory = category => {
    let arr = Object.values(require('auto-load')('src/commands/' + category.name));
    arr.forEach(elem => {
        elem.category = category.name;
    });
    commands.push(...arr);
}

Categories.forEach(loadCategory);

setInterval(() => {
    const home_general = Hammer.client.channels.get(Settings.spam_channel);
    home_general.fetchMessages({limit: 1}).then(messages => {
        let msg = messages.first();
        if(new Date() - msg.createdAt > (msg.author === Hammer.self ? 5 : 2) * 3600 * 1000) {
            if(Math.random() < 0.2) {
                home_general.send('Did you know...?');
                module.exports.exec(msg, 'trivia');
            }
        }
    }).catch(console.error);
}, 300000);
