// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Discord = require('discord.js');
const Hammer = require('./hammer.js');
const Categories = require('../commands/categories.json');
const Settings = require('../config/settings.json');
const Tools = require('../util/tools.js');

let regexes = Object.values(require('auto-load')('src/regex'));

module.exports.parse = msg => {
    return new Promise(resolve => {
        if(msg.author.bot) {
            return false;
        }

        // This controls whether any regex has triggered.
        // Makes command parser not complain if a comman is incorrect.
        let found = false;

        regexes.forEach(regex => {
            if(regex.regex) {
                let finder = regex.regex.some(elem => {
                    return elem.test(msg);
                });
                if(finder) {
                    let allowed = true;
                    if(regex.mention_only && msg.channel.type !== 'dm') {
                        allowed = new RegExp('(' + Settings.name + '|' + Tools.exclamatedMention(Hammer.self) + '|' + Hammer.self.toString() + ')').test(msg.content);
                    }
                    found = found || allowed;
                    if(allowed) {
                        if(regex.exec_always) {
                            regex.exec_always(msg);
                            found = true;
                        }
                        if(regex.exec_chance && regex.probability && Math.random() <= regex.probability) {
                            regex.exec_chance(msg);
                            found = true;
                        }
                    }
                }
            }
        });
        resolve(found);
    });
}
