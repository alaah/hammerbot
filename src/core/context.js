// hammerbot: Dead Cells Discord bot
// Copyright (C) 2018 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Settings = require('../config/settings.json');
const Server = require('../config/server.json');
const Discord = require('discord.js');
const {Client} = require('pg');
const client = new Discord.Client();

module.exports = {
    client: client,
    postgres: new Client({
        connectionString: process.env.DATABASE_URL,
        ssl: Settings.ssl
    }),
    channels: {},
    init: () => {
        module.exports.self = module.exports.client.user;
        module.exports.guild = module.exports.client.guilds.find(val => val.id === Server.id);
        if(!module.exports.guild) {
            throw 'Cannot find the server.';
        }
        module.exports.channels.general = module.exports.client.channels.find(val => val.id === Server.general_channel);
        module.exports.channels.bug = module.exports.client.channels.find(val => val.id === Server.bug_channel);
        module.exports.channels.suggestion = module.exports.client.channels.find(val => val.id === Server.suggestion_channel);
        if(Settings.pg) {
            module.exports.postgres.connect();
        }
    }
};
