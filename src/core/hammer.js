// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Settings = require('../config/settings.json');
const Discord = require('discord.js');
const {Client} = require('pg');

module.exports = {
    client: new Discord.Client(),
    postgres: new Client({
        connectionString: process.env.DATABASE_URL,
        ssl: Settings.ssl
    }),
    channels: {},
    init: () => {
        module.exports.self = module.exports.client.user;
        module.exports.error_channel = module.exports.client.channels.get(Settings.error_channel)
        module.exports.postgres.connect();
    }
};
