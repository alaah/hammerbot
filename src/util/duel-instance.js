// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const colors = require('../config/colors.json');
const Hammer = require('../core/hammer.js');
const LoadIcon = require('./load-icon.js');
const DuelMagick = require('./duel-magick.js');
const Tools = require('./tools.js');

const standard_duration = 300;

let ongoingDuels = [];

const commonAdjectives = ['Foolish', 'Excellent', 'Deathly', 'Deadly', 'Smart', 'Intelligent', 'Sweet', 'Official', 'Officious', 'Cannibalistic', 'Unepic', 'Terrific', 'French', 'Holy', 'Epic', 'E🅱ig', 'Hollow', 'Legendary', 'Primal', 'Cursed', 'Deprecated', 'Indie', 'Corporate', 'Soft', 'Hard', 'Deep', 'Hazardous', 'Fortnite', 'Unexpected', 'Unbelievable', 'Nice', 'Special', 'Idiotic', 'Independent', 'Hardy', 'Incompetent', 'Impotent', 'Condemned', 'Great', 'Mild', 'Amazing', 'Incredible', 'Crazy', 'Marvelous', 'Believable', 'Poor', 'Strong', 'Weak', 'Plausible', 'Dumb', 'Epic-ish', 'Elite', 'Shallow', 'Glorious', 'Bloody', 'Toxic', 'Swift', 'Exquisite', 'Caustic', 'Risible', 'Laughable', 'Platitudinous', 'Nefarious', 'Wicked', 'Native', 'Ancient'];

const duelAdjectives = ['Rainbow', 'Pretty', 'Colorful', 'Fortnite', 'Misty', 'Burning', 'Exciting', 'Huge'];

const humanAdjectives = ['Old', 'Disgusting', 'Guileless', 'Zealous', 'Famous', 'Adroit', 'Resourceful', 'Naive', 'Clingy', 'Dead', 'Cynical', 'Reliable', 'Grumpy', 'Unreliable', 'Witty', 'Crimson', 'Sincere', 'Sympathetic', 'Courageous', 'Ambitious', 'Brave', 'Reckless', 'Adventurous', 'Lucky', 'Cunning', 'Unhinged', 'Baka', 'Frontline', 'Arch', 'Alpha', 'Undead'];
const humanNouns = ['Zombies', 'Inquisitors', 'Elites', 'Fools', 'Idiots', 'Archers', 'Sandals', 'Frontlines', 'Wizards', 'Battle Mages', 'Heralds', 'Guardians', 'Crusaders', 'Archons', 'Legends', 'Ancients', 'Divinities', 'Immortals', 'Priests', 'Mages', 'Crimson Demons', 'Thieves', 'Bananas', 'Monkeys', 'Collectors', 'Bombers', 'Arbiters', 'Horses', 'NEETs', 'Weebs', 'Warriors', 'Conquerors', 'Swordsmen', 'Chat Members', 'Friends', 'Ghosts', 'Special Friends', 'Catapults', 'Neypahs', 'Watchers', 'Hands of the King', 'Assassins', 'Keepers', 'Time Keepers', 'Golems', 'Guardians', 'Giants', 'Midgets', 'Kings', 'Keepers', 'Incomplete Ones', 'Cowards', 'Rangers', 'Dead Brain Cells', 'Knights', 'Biters', 'Shieldbearers', 'Phazers', 'Defenders', 'Catchers', 'Cleavers', 'Kamikazes', 'Scorpions', 'Worms', 'Spikers', 'Mushrooms', 'Plants', 'Slashers', 'Lolis', 'Shockers', 'Thornies', 'Foggers', 'Spinners', 'Bombardiers', 'Casters', 'Lancers', 'Screamers', 'Cannibals', 'Redditors', 'Steam Forums users', 'Elkopolos', 'Gods', 'Baguettes', 'Croissants', 'Ramparts', 'Unicorns', 'Architects', 'Warchasers'];
const placeNouns = ['Sunshine', 'Night', 'Hammer', 'Motion Twin', 'Prison', 'Sewers', 'Graveyard', 'Cavern', 'Clock Tower'];

const coreCategories = ['Sword', 'Shield', 'Knife', 'Bow', 'Gun', 'Crossbow', 'Turret', 'Food', 'Ice', 'Fire'];
const coreNouns = ['Argument', 'Fencing', 'Bout', 'Competition', 'Strife', 'Symphony', 'Discord', 'Disagreement', 'Contest', 'Conflict', 'Feud', 'Flame War', 'Encounter', 'Duel', 'Quarrel', 'Disunity', 'Corruption', 'Meeting', 'Blessing', 'Hazard', 'Binding', 'Brawl', 'Trial', 'Battle', 'Battle Royale', 'Fight', 'Confrontation', 'Affair', 'Challenge', 'Engagement', 'Combat', 'Shooting'];

class DuelInstance {
    constructor(channel, user1, user2, options) {
        this.channel = channel;
        this.done = false;
        this.choice = [
            {
                color: null,
                item: null
            },
            {
                color: null,
                item: null
            }
        ];
        this.called = [false, false];
        this.user = [user1, user2];
        this.applyOptions(options);
        this.name = 'The ';
        this.custom_item = null;
        let rando = Math.random();

        let addNamePrefix = () => {
            this.name += Tools.arrayPick(commonAdjectives.concat(duelAdjectives)) + ' ';
        };

        let addCoreName = () => {
            if(Math.random() < 0.1) {
                this.name += Tools.arrayPick(coreCategories) + ' ';
            } else if(this.options.fantasy && Math.random() < 0.2) {
                // To uppercase and underscores to spaces
                let icon = LoadIcon.random();
                this.custom_item = icon;
                this.name += LoadIcon.dirToName(icon) + ' ';
            }
            this.name += Tools.arrayPick(coreNouns);
        };

        let addNameAffix = () => {
            this.name += ' of the ';
            if(Math.random() < 0.6) {
                this.name += Tools.arrayPick(humanNouns.concat(placeNouns));
            } else if(Math.random() < 0.3) {
                this.name += Tools.arrayPick(commonAdjectives.concat(humanAdjectives));
            } else {
                this.name += Tools.arrayPick(commonAdjectives.concat(humanAdjectives)) + ' ' + Tools.arrayPick(humanNouns);;
            }
        };

        if(rando < 1/5) {
            addNamePrefix();
            addCoreName();
            addNameAffix();
        } else if(rando < 3/5) {
            addNamePrefix();
            addCoreName();
        } else {
            addCoreName();
            addNameAffix();
        }

        this.id = ongoingDuels.length;
        this.timeout = setTimeout(() => {
            if(!this.called[0]) {
                this.channel.send(this.name + ': ' + this.user[0].username + ' timed out.');
            }
            if(!this.called[1]) {
                this.channel.send(this.name + ': ' + this.user[1].username + ' timed out.');
            }
            this.deleteThis();
        }, standard_duration * 1000);

        this.channel.send(this.name + ' has begun!').then(message => setTimeout(() => {message.delete()}, 5000)).catch(console.error);
        ongoingDuels.push(this);
        DuelMagick.genText(this.name, this.id);


        for(let i = 0; i < 2; ++i) {
            Hammer.postgres.query(`select * from lead where userid='${this.user[i].id}' and serverid='${this.channel.guild.id}'`, (err, res) => {
                if(!res || !res.rows || res.rows.length === 0) {
                    Hammer.postgres.query(`insert into lead(userid, score, games, serverid) values('${this.user[i].id}',0,0,'${this.channel.guild.id}')`, (err, res) => {
                        if(err) {
                            console.log(err);
                        }
                    });
                }
            });
        }
    }

    get duels() {
        return ongoingDuels;
    }

    static get duels() {
        return ongoingDuels;
    }

    deleteThis(announce) {
        if(announce) {
            this.channel.send(this.name + ' has been cancelled.');
        }
        if(this.timeout) {
            clearTimeout(this.timeout);
            this.timeout = null;
        }
        ongoingDuels.splice(ongoingDuels.indexOf(this), 1);
    }

    applyOptions(options) {
        this.options = {
            fantasy: false,
            nohelp: false,
            unranked: false,
            doublestake: false,
            truerps: false
        };

        if(typeof options === 'string') {
            if(options.indexOf('f') !== -1) {
                this.options.fantasy = true;
            }
            if(options.indexOf('n') !== -1) {
                this.options.nohelp = true;
            }
            if(options.indexOf('u') !== -1) {
                this.options.unranked = true;
            } else if(options.indexOf('d') !== -1) {
                this.options.doublestake = true;
            }
            if(options.indexOf('t') !== -1) {
                this.options.truerps = true;
            }
        }
    }

    static findDuel(user) {
        let result = null;
        ongoingDuels.forEach(duel => {
            if(duel.user[0] === user || duel.user[1] === user) {
                result = duel;
            }
        });
        return result;
    }

    call(context, color, item) {
        let player = 1
        if(context.author === this.user[0]) {
            player = 0;
        }

        if(color <= 2 || !this.options.truerps) {
            if(color === 3 && Math.random() < 0.5) {
                color = 4;
            }

            let icon = null;
            if(item) {
                icon = LoadIcon.load(item);
                if(icon) {
                    if(color < 3 && icon.indexOf(colors[color].color) === -1 && !this.options.fantasy) {
                        let index = icon.lastIndexOf('/');
                        context.channel.send(LoadIcon.dirToName(icon) + ' is ' + icon.substr(0, index) + '!');
                        icon = null;
                    }
                } else {
                    context.channel.send('Unknown icon.');
                }
            } else {
                icon = LoadIcon.random(colors[color].color);
            }

            if(icon) {
                this.called[player] = true;
                this.choice[player] = {
                    color: color,
                    item: icon
                };
                context.channel.send('You\'ve chosen a ' + colors[color].color + ' ' + LoadIcon.dirToName(icon) + '!');
            }

            ////////////////////////////////////
            //  Hammer cheat
            ////////////////////////////////////
            if(this.user[1].bot) {
                this.called[1] = true;

                if(color < 3) {
                    if(color === 0) {
                        this.choice[1].color = 1;
                    } else if(color === 1) {
                        this.choice[1].color = 2;
                    } else {
                        this.choice[1].color = 0;
                    }
                } else {
                    this.choice[1].color = 4;
                }

                if(this.options.fantasy && this.custom_item) {
                    this.choice[1].item = this.custom_item;
                } else {
                    this.choice[1].item = LoadIcon.random(colors[this.choice[1].color].color);
                }

                // Randomly fuck your legendary
                if(Math.random() < 0.65 && this.choice[0].color === 4) {
                    this.choice[0].color = 3;
                }
            }

            if(this.called[0] && this.called[1]) {
                DuelMagick.convert(this.choice[0], this.id + '0', false).then(() => {
                    DuelMagick.convert(this.choice[1], this.id + '1', true).then(() => {
                        DuelMagick.montage(this.user[0], this.user[1], this.id, this.name).then(() => {
                            DuelMagick.montageFinal(this.id).then(() => {
                                this.channel.send({
                                    files: [{
                                        attachment: 'out' + this.id + '.png',
                                        name: this.name + '.png'
                                    }]
                                }).then(() => {
                                    this.announceWinner();
                                }).catch(console.error);
                            }).catch(console.error);
                        }).catch(console.error);;
                    }).catch(console.error);
                }).catch(console.error);
            }
        } else {
            context.channel.send('This color is disabled.');
        }
    }

    sendNotice(context, is_challenger) {
        this.user[is_challenger ? 0 : 1].createDM().then(dm => {
            let options_info = 'Options: ';
            let print_options = false;
            if(this.options.fantasy) {
                options_info += 'fantasy items, ';
                print_options = true;
            }
            if(this.options.doublestake) {
                options_info += 'double stake, ';
                print_options = true;
            }
            if(this.options.truerps) {
                options_info += 'true RPS, ';
                print_options = true;
            }
            if(!print_options) {
                options_info = '';
            } else {
                options_info = options_info.substr(0, options_info.length - 2);
            }

            if(is_challenger) {
                if(this.user[1].bot) {
                    dm.send(
                        {files: [{
                             attachment: 'files/rps/fight-me.mp3',
                             name: 'fightme.mp3'
                        }]
                    });
                } else {
                    dm.send('You have challenged ' + this.user[1].tag + ' to ' + this.name + '! ' + options_info);
                }
            } else {
                dm.send(this.user[0].tag + ' has challenged you to ' + this.name + '! ' + options_info);
            }

            let msg = 'How to play:\nReply to this DM with the color of your choice and optionally an item, for example `survival bloodshield` or just `red`. If you don\'t specify the item, it will be randomed.\n';
            if(!this.options.truerps) {
                msg += 'In this mode, you can also select `colorless` which will either be cursed or legendary. The former always loses, the latter always wins.\n'
            }
            msg += 'You have limited time, so don\'t hesitate. See `help duel` for more details and `!listicons` for all the available items.\n'
            msg += 'Colors are as follows:';
            if(!this.options.nohelp) {
                dm.send(msg,
                    {files: [{
                         attachment: 'files/rps/rps.png',
                         name: 'rps.png'
                    }]
                });
            }
        });
    }

    alterUser(user, score) {
        if(score !== 0) {
            Hammer.postgres.query(`update lead set score=score+${score} where userid='${this.user[user].id}' and serverid='${this.channel.guild.id}'`, (err, res) => {
                if(err) {
                    console.log(err);
                }
            });
        }
    }

    addToCounter(user) {
        Hammer.postgres.query(`update lead set games=games+1 where userid='${this.user[user].id}' and serverid='${this.channel.guild.id}'`, (err, res) => {
            if(err) {
                console.log(err);
            }
        });

        Hammer.postgres.query(`select games from lead where userid='${this.user[user].id}' and serverid='${this.channel.guild.id}'`, (err, res) => {
            if(res.rows[0] && res.rows[0].games % 5 === 0) {
                Hammer.postgres.query(`update lead set score=score+1 where userid='${this.user[user].id}' and serverid='${this.channel.guild.id}'`, (err, res) => {
                    if(err) {
                        console.log(err);
                    }
                });
                this.channel.send(this.user[user].username + ' gains a point for activity!');
            }
            if(err) {
                console.log(err);
            }
        });
    }

    announceWinner() {
        let pts = this.options.doublestake ? 2 : 1;
        if(this.user[1].bot) {
            pts = 0;
        }
        if(this.options.unranked) {
            pts = 0;
        }

        let win = (player, item) => {
            this.channel.send(this.user[player].username + (item ? ' uses the correct item and' : '') + ' wins ' + this.name + '!');
            this.alterUser(player ? 1 : 0, pts);
            this.alterUser(player ? 0 : 1, -pts);
        };

        let tie = () => {
            this.channel.send(this.name + ' ended in a tie!');
        };

        if(this.choice[0].color === this.choice[1].color) {
            if(this.custom_item) {
                if(this.choice[0].item !== this.custom_item && this.choice[1].item === this.custom_item) {
                    win(1, true);
                } else if(this.choice[0].item === this.custom_item && this.choice[1].item !== this.custom_item) {
                    win(0, true);
                } else {
                    tie();
                }
            } else {
                tie();
            }
        } else if(this.choice[0].color === 3 || this.choice[1].color === 4){
            win(1);
        } else if(this.choice[0].color === 4 || this.choice[1].color === 3){
            win(0);
        } else {
            let count = this.choice[0].color - this.choice[1].color;
            if(count === -2 || count === 1 || count === 0) {
                win(0);
            } else {
                win(1);
            }
        }

        if(pts !== 0) {
            this.addToCounter(0);
            this.addToCounter(1);
        }

        // A little time to rest and drink a glass of water.
        setTimeout(() => {
            this.deleteThis();
        }, 5000);
    }
}

module.exports = DuelInstance;
