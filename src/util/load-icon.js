// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Discord = require('discord.js');
const Fs = require('fs');
const Tools = require('./tools.js');

const basedir = 'files/gear';
const subdirs = ['white', 'red', 'purple', 'green', 'red-purple', 'purple-green', 'green-red'];
let icons = [];
let red = [];
let purple = [];
let green = [];
let white = [];

subdirs.forEach(subdir => {
    Fs.readdir('files/gear/' + subdir, (err, files) => {
        if(err) {
            throw err;
        } else {
            files.forEach(file => {
                icons.push(subdir + '/' + file);
                if(subdir.indexOf('purple') !== -1) {
                    purple.push(subdir + '/' + file);
                }
                if(subdir.indexOf('green') !== -1) {
                    green.push(subdir + '/' + file);
                }
                if(subdir.indexOf('red') !== -1) {
                    red.push(subdir + '/' + file);
                }
                if(subdir === 'white') {
                    white.push(subdir + '/' + file);
                }
            });
        }
    });
});

const compare = (a, b) => {
    a = a.substr(a.lastIndexOf('/'));
    b = b.substr(b.lastIndexOf('/'));
    return a == b ? 0 : a < b ? -1 : 1;
};

red.sort(compare);
purple.sort(compare);
green.sort(compare);
// White doesn't need sorting cause it ain't split into several subdirs.

module.exports.dirToName = icon => {
    let index = icon.lastIndexOf('/');
    if(index !== -1) {
        return icon.substr(index + 1, icon.length - index - 5).replace(/_/gi, ' ').replace(/\b\w/g, l => l.toUpperCase());
    } else {
        return icon;
    }
}

module.exports.list = () => {
    let embed = new Discord.RichEmbed();
    embed.setTitle('Icon list');
    embed.setDescription('Pick a name and write a unique part of it, removing any whitespace.');
    let rando = Math.random();
    embed.setColor(rando < 0.33 ? 'ec2424'
                 : rando < 0.66 ? 'a051ff'
                 : '17ce75');
    let addField = (arr, name) => {
        let field = ''
        arr.forEach(icon => {
            field += module.exports.dirToName(icon) + ', ';
        });
        field = field.substr(0, field.length - 2);
        embed.addField(name, field);
    };
    addField(red, 'Brutality');
    addField(purple, 'Tactics');
    addField(green, 'Survival');
    addField(white, 'Colorless');

    return embed;
}

module.exports.random = color => {
    if(color === 'red') {
        return Tools.arrayPick(red);
    }
    if(color === 'green') {
        return Tools.arrayPick(green);
    }
    if(color === 'purple') {
        return Tools.arrayPick(purple);
    }
    return Tools.arrayPick(icons);
}

module.exports.load = name => {
    let file = '';
    icons.forEach(icon => {
        if(icon.indexOf(name) !== -1) {
            file = icon;
        }
    });
    if(file === '') {
        icons.forEach(icon => {
            if(icon.replace(/_/gi, '').indexOf(name) !== -1) {
                file = icon;
            }
        });
    }
    return file === '' ? null : file;
}
