// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Hammer = require('../core/hammer.js');
const Server = require('../core/server.js');

module.exports = (context, id) => {
    return new Promise((resolve, reject) => {
        Hammer.postgres.query(`select * from data where serverid='${context.server.id}' and id=${id}`, (err, res) => {
            if(err) {
                console.error('Whoops. ' + err.stack);
                reject('Unable to query.');
            } else if(res.rows.length > 0) {
                context.server.get_channel(res.rows[0].type).channel.fetchMessage(res.rows[0].msgid)
                .then(parsed => resolve({id: id, post: parsed, row: res.rows[0]}))
                .catch(error => {
                    console.error(error);
                    reject('Unexpected error. Message alaah.');
                });
            } else {
                reject('The id could not be found.');
            }
        });
    });
}
