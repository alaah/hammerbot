// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Discord = require('discord.js');
const Hammer = require('../core/hammer.js');
const Server = require('../core/server.js');

module.exports = {
    formattedDate: () => {
        let now = new Date();
        year = '' + now.getFullYear();
        month = '' + (now.getMonth() + 1); if (month.length == 1) { month = '0' + month; }
        day = '' + now.getDate(); if (day.length == 1) { day = '0' + day; }
        hour = '' + now.getHours(); if (hour.length == 1) { hour = '0' + hour; }
        minute = '' + now.getMinutes(); if (minute.length == 1) { minute = '0' + minute; }
        second = '' + now.getSeconds(); if (second.length == 1) { second = '0' + second; }
        return year + '-' + month + '-' + day + ' ' + hour + ':' + minute + ':' + second;
    },
    getSubmissionColor: num => {
        switch(num) {
            case 0: return 0x159015;
            case 1: return 0x3510e1;
            case 2: return 0xdd2e44;
            case 3: return 0xbaff49;
            case 4: return 0xf4bdc5;
            default: return 0x252525;
        }
    },
    getSubmissionTitle: num => {
        switch(num) {
            case 0: return 'Suggestion';
            case 1: return 'Meta suggestion';
            case 2: return 'Bug report';
            case 3: return 'Approved suggestion';
            case 4: return 'Approved bug report';
            default: return 'Unknown';
        }
    },
    formattedAuthorTag: msg => {
        if(msg.channel.type === 'dm') {
            return msg.author.tag;
        } else {
            if(msg.member.displayName !== msg.author.username) {
                return msg.member.displayName + ' (' + msg.author.tag + ')';
            } else {
                return msg.author.tag;
            }
        }
    },
    removeFieldsBy: (sourcefields, usertag) => {
        let new_fields = [];
        sourcefields.forEach(field => {
            if(field.name !== 'Note by ' + usertag) {
                new_fields.push(field);
            }
        });
        return new_fields;
    },
    getScore: (msg, row) => {
        let score = 0;
        let a = null, b = null;

        // Legacy thumbsup/down emojis
        a = msg.reactions.find(val => val.emoji.name === '👍');
        b = msg.reactions.find(val => val.emoji.name === '👎');

        if(a && b) {
            score += a.count;
            score -= b.count;
        }

        // Regular up/down
        a = msg.reactions.find(val => val.emoji === Server.home().upboat);
        b = msg.reactions.find(val => val.emoji === Server.home().downboat);

        if(a && b) {
            score += a.count;
            score -= b.count;
        }

        // Custom up/down
        a = msg.reactions.find(val => val.emoji === Server.from(msg).upboat);
        b = msg.reactions.find(val => val.emoji === Server.from(msg).downboat);

        if(score === 0 && a && b) {
            score += a.count;
            score -= b.count;
        }

        return score;
    },

    arrayPick: arr => {
        return arr.length >= 1 ? arr[Math.floor(Math.random() * arr.length)] : null;
    },
    mentionToID: mention => {
        return mention.replace(/[\\<>@#&!]/g, '');
    },
    getUserFromMention: mention => {
        return new Promise((resolve, reject) => {
            Hammer.client.fetchUser(module.exports.mentionToID(mention)).then(user => {
                resolve(user);
            }).catch(() => {
                // Didn't fetch, then it's not a valid mention
                let result = Hammer.client.users.find(user => user.tag === mention);
                if(result) {
                    resolve(result);
                } else {
                    reject('User not found.');
                }
            });
        });
    },
    markAlias: (string, alias) => {
        let pos = string.indexOf(alias);
        return pos === -1 ? string : (string.slice(0, pos) + '[' + string.slice(pos, pos + 1) + ']' + string.slice(pos + 1));
    },
    exclamatedMention: user => {
        return user.toString().substr(0, 2) + '!' + user.toString().substring(2);
    }
};
