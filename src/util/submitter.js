// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Hammer = require('../core/hammer.js');
const Tools = require('./tools.js');
const Server = require('../core/server.js');

const makeEmbed = (msg, text, color, title, id) => {
    return {
        embed: {
            color: parseInt(color, 16),
            author: {
                name: Tools.formattedAuthorTag(msg),
                icon_url: msg.author.avatarURL
            },
            title: title,
            description: text,
            footer: {
                text: 'id: ' + id
            },
            image: msg.attachments.first() ? {
                url: msg.attachments.first().url
            } : null
        }
    }
}

module.exports = (msg, context, text, data) => {
    return new Promise((resolve, reject) => {
        let currid;
        Hammer.postgres.query(`select * from data where serverid='${context.server.id}' order by id desc limit 1`, (err, res) => {
            if(err) {
                console.error(err);
                reject('Unexpected error.');
            } else {
                if(res.rows.length === 0) {
                    currid = 0;
                } else {
                    currid = res.rows[0].id + 1;
                }
                data.channel.send(makeEmbed(msg, text, data.color1, data.name, currid)).then(() => {
                    resolve('Successfully submitted with id ' + currid + '.');
                    let last = Hammer.client.user.lastMessage;
                    let id1 = context.author.id;
                    let id2 = last.id;
                    if(data.is_suggestion) {
                        last.react(context.server.downboat).then(last.react(context.server.upboat));
                    }
                    Hammer.postgres.query(`insert into data (id, userid, msgid, type, serverid) values(${currid},${id1},${id2},${data.id},${context.server.id});`, (err, res) => {
                        if(err) {
                            console.error('Failed to upload to db');
                            console.error(err);
                            console.error('(' + currid + ',' + id1 + ',' + id2 + ')');
                        }
                    });
                }).catch(console.error);
            }
        });
    });
};
