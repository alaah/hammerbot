// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Settings = require('../config/settings.json');
const Hammer = require('../core/hammer.js');

module.exports = {
    isMaster: msg_or_user => {
        if(msg_or_user.user) {
            return msg_or_user.user.id === Settings.master;
        } else if(msg_or_user.id) {
            return msg_or_user.id === Settings.master;
        } else {
            return false;
        }
    },

    isAdmin: msg => {
        if(msg.author.id === Settings.master) {
            return true;
        }
        if(msg.channel.type === 'dm') {
            return false;
        }
        return msg.member.hasPermission('ADMINISTRATOR')
    },

    isPrivileged: context => {
        if(context.author.id === Settings.master) {
            return true;
        }
        if(context.channel.type === 'dm') {
            return false;
        }
        return context.member.hasPermission("MANAGE_MESSAGES");
    },

    isVerified: msg => {
        if(msg.channel.type === 'dm') {
            return false;
        }
        return msg.member.highestRole.name !== '@everyone';
    }
}
