// hammerbot: Dead Cells Discord bot
// Copyright 2019 alaah

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

const Magick = require('simple-imagemagick');
const colors = require('../config/colors.json');

const dir = 'files/gear/';

module.exports.convert = (choice, id, flip) => {
    return new Promise((resolve, reject) => {
        if(flip) {
            Magick.convert([
                dir + choice.item,
                '-background', colors[choice.color].background,
                '-flop',
                '-extent', '88x88-8-8',
                '-bordercolor', colors[choice.color].border,
                '-compose', 'copy',
                '-border', '2x2',
                '{mpr:image' + id + '}'
            ], (err, stdout) => {
                if(err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        } else {
            Magick.convert([
                dir + choice.item,
                '-background', colors[choice.color].background,
                '-extent', '88x88-8-8',
                '-bordercolor', colors[choice.color].border,
                '-compose', 'copy',
                '-border', '2x2',
                '{mpr:image' + id + '}'
            ], (err, stdout) => {
                if(err) {
                    reject(err);
                } else {
                    resolve();
                }
            });
        }
    });
};

module.exports.genText = (label, id) => {
    return new Promise((resolve, reject) => {
        Magick.convert([
            '-fill', 'white',
            '-font', 'Roboto-Light',
            '-size', '990x90',
            '-background', 'transparent',
            '-gravity', 'center',
            'label:' + label,
            'lavel' + id + '.png'
        ], (err, stdout) => {
            if(err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

module.exports.montageFinal = id => {
    return new Promise((resolve, reject) => {
        Magick.convert([
            '-append',
            'lavel' + id + '.png',
            'out' + id + '.png',
            'out' + id + '.png'
        ], (err, stdout) => {
            if(err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};

module.exports.montage = (user1, user2, id, title) => {
    return new Promise((resolve, reject) => {
        Magick.montage([
            'null:',
            'null:',
            '\(', '{mpr:image' + id + '0' + '}', '-scale', '200%', '-set', 'label', user1.username, '\)',
            'files/rps/vs.png',
            '\(', '{mpr:image' + id + '1' + '}', '-scale', '200%', '-set', 'label', user2.username, '\)',
            'null:',
            'null:',
            '-font', 'Roboto-Light',
            '-fill', 'white',
            '-pointsize', '40',
            '-geometry', '+35+5',
            '-tile', '7x2',
            '-background', 'none',
            'out' + id + '.png'
        ], (err, stdout) => {
            if(err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
};
